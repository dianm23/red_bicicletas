var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicletaModel');

describe('Testing Bicicletas', function () {
    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error: '));
        db.once('open', function () {
            console.log('conexion exitosa BD');
            done();
        });
    });

    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            mongoose.disconnect(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('crea instancia Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "azul", "urbana", [4.570868, -74.58]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("azul");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(4.570868);
            expect(bici.ubicacion[1]).toEqual(-74.58);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });


    describe('Bicicleta.add', () => {
        it('agregar bici', (done) => {
            var aBici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
            Bicicleta.add(aBici, function (err, newBici) {
                if (err) console.log(err);
                Bicicleta.allBicis(function (err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);

                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('Devolver bici code 1', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);
                var a = new Bicicleta({ code: 1, color: "azul", modelo: "urbana" });
                Bicicleta.add(a, function (err, newBici) {
                    if (err) console.log(err);

                    var b = new Bicicleta({ code: 2, color: "verde", modelo: "urbana" });
                    Bicicleta.add(b, function (err, newBici) {
                        if (err) console.log(err);

                        Bicicleta.findByCode(1, function (err, bici) {
                            expect(bici.code).toBe(a.code);
                            expect(bici.color).toBe(a.color);
                            expect(bici.modelo).toBe(a.modelo);
                            done();
                        });
                    });
                });
            });
        });
    });

    describe('Bicicleta.removeByCode', () => {
        it('Borrar bici code 1', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);
                var a = new Bicicleta({ code: 1, color: "azul", modelo: "urbana" });
                Bicicleta.add(a, function (err, newBici) {
                    if (err) console.log(err);

                    var b = new Bicicleta({ code: 2, color: "verde", modelo: "urbana" });
                    Bicicleta.add(b, function (err, newBici) {
                        if (err) console.log(err);
                        // Borramos el code=1
                        Bicicleta.removeByCode(1, function (err, bici) {
                            if (err) console.log(err);
                            // Buscamos el que acabamos de borrar
                            Bicicleta.findByCode(1, function (err, bici) {
                                if (err) console.log(err);
                                // La busqueda debera devolver null
                                expect(bici).toBe(null);
                                done();
                            });
                        });
                    });
                });
            });
        });
    });

    describe('UpdateByCode', () => {
        it('actualiza datos bici', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                if (err) console.log(err);
                expect(bicis.length).toBe(0);
                var a = new Bicicleta({ code: 1, color: "azul", modelo: "urbana" });
                Bicicleta.add(a, function (err, newBici) {
                    if (err) console.log(err);
                    Bicicleta.allBicis(function (err, bicis) {
                        if (err) console.log(err);
                        expect(bicis.length).toBe(1);
                        Bicicleta.findByCode(1, function (err, bici) {
                            if (err) console.log(err);
                            expect(bici.color).toBe("azul");
                            var b = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
                            Bicicleta.updateByCode(b, () => {
                                Bicicleta.findByCode(1, function (err, bici) {
                                    if (err) console.log(err);

                                    expect(bici.code).toBe(1);
                                    expect(bici.color).toBe("azul");
                                    expect(bici.modelo).toBe("urbana");
                                    done();
                                });
                            });
                        });
                    });
                });
            });
        });
    });

});


/*beforeEach(() => { Bicicleta.allBicis = []; });

describe('Bicicleta.allBicis', () => {
    it('sin bicis', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () => {
    it('una bici', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'azul', 'urbana', [4.570868, -74.58]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findByID', () => {
    it('devuelve bici id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1, "azul", "urbana");
        var aBici2 = new Bicicleta(2, "verde", "urbana");
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);

        var targetBici = Bicicleta.findByID(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);
    });
});


describe('Bicicleta.removeById', () => {
    it('bici no encontrada con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);


        var aBici = new Bicicleta(1, "azul", "urbana");
        var bBici = new Bicicleta(2, "verde", "urbana");
        Bicicleta.add(aBici);
        Bicicleta.add(bBici);
        var idDelete = 1;
        Bicicleta.removeById(idDelete);
        expect(function () { Bicicleta.findByID(idDelete) }).toThrow(new Error(`No existe bicicleta con el id ${idDelete}`));
    });
});*/