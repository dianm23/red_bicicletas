var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicletaModel');
var server = require('../../bin/www');
var request = require('request');

var base_url = "http://localhost:5000/api/bicicletas";

describe('Bicicleta API', () => {
    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function () {
            console.log('Conexion Exitosa BD');
            done();
        });
    });

    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            mongoose.disconnect(err);
            done();
        });
    });

    describe('GET BICICLETA API /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function (error, response, body) {
                var bici = JSON.parse(body);
                console.log(bici);
                expect(response.statusCode).toBe(200);
                expect(bici.length).toBe(0);
                done();
            });
        });
    });

    describe('POST BICICLETA API /create', () => {
        it('Status 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var data = '{"id": 1, "color":"azul", "modelo": "urbana", "lat":4.57, "lng":-74.58}';
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: data
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicletas;
                expect(bici.color).toBe("azul");
                expect(bici.ubicacion[0]).toBe(10.46);
                expect(bici.ubicacion[1]).toBe(-73.24);
                done();
            });
        });
    });

    /*
        describe('POST BICICLETAS /create', () => {
            describe('POST BICICLETAS /', () => {
                it('Status 200', (done) => {
                    var header = { 'content-type': 'application/json' };
                    var data = '{"id":3, "color":"rojo", "modelo":"urbano", "lat": -34, "lng": -54}';
                    request.post({
                        headers: header,
                        url: 'http://localhost:5000/api/bicicletas/create',
                        body: data
                    }, function (error, response, body) {
                        expect(response.statusCode).toBe(200);
                        expect(Bicicleta.findByID(3).color).toBe("rojo");
                        done();
                    });
                });
            });
        });
    
    
        describe('PUT BICICLETA API /update', () => {
            it('Status 200', (done) => {
                var headers = { 'content-type': 'application/json' };
                var data = '{"id":1,"color":"marron","modelo":"4x4","lat":10.48,"lng":-73.24}';
    
                request.post({
                    headers: headers,
                    url: 'http://localhost:5000/api/bicicletas/create',
                    body: data
                }, function (error, response, body) {
                    expect(response.statusCode).toBe(200);
    
                    var dat = '{"id_find": 1, "id":5,"color":"morado","modelo":"urbano","lat": 13.58,"lng": -78}';
    
                    request.put({
                        headers: headers,
                        url: 'http://localhost:5000/api/bicicletas/update',
                        body: dat
                    }, function (error, response, body) {
                        expect(response.statusCode).toBe(200);
                        var res = JSON.parse(body).bicicleta;
                        expect(res.color).toBe("morado");
                        done();
                    });
                });
    
            });
        });
    
    
        describe('DELETE BICICLETA API /delete', () => {
            it('Status 200', (done) => {
                var headers = { 'content-type': 'application/json' };
                var data = '{"id":1,"color":"marron","modelo":"4x4","lat":10.48,"lng":-73.24}';
    
                request.post({
                    headers: headers,
                    url: 'http://localhost:5000/api/bicicletas/create',
                    body: data
                }, function (error, response, body) {
                    expect(response.statusCode).toBe(200);
    
                    request.delete({
                        headers: headers,
                        url: 'http://localhost:5000/api/bicicletas/delete',
                        body: data
                    }, function (error, response, body) {
                        expect(response.statusCode).toBe(204);
                        done();
                    });
                });
            });
    */
});